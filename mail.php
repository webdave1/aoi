<?
  //Klasse einbinden
  require('phpmailer/class.phpmailer.php');
  
  //Instanz von PHPMailer bilden
  $mail = new PHPMailer();
 
  //Absenderadresse der Email setzen
  $mail->From = "aze@avionic-design.de";
  
  //Name des Abenders setzen
  $mail->FromName = "AZE";
  
  //Empfängeradresse setzen
  $mail->AddAddress("david.avionic-design.de");
  
  //Betreff der Email setzen
  $mail->Subject = "Die erste Mail";
 
  //Text der EMail setzen
  $mail->Body = "Hallo! \n\n Dies ist die 2. Email mit PHPMailer!";
  
  //EMail senden und überprüfen ob sie versandt wurde
  if(!$mail->Send())
  {
     //$mail->Send() liefert FALSE zurück: Es ist ein Fehler aufgetreten
     echo "Die Email konnte nicht gesendet werden";
     echo "Fehler: " . $mail->ErrorInfo;
  }
  else
  {
     //$mail->Send() liefert TRUE zurück: Die Email ist unterwegs
     echo "Die Email wurde versandt.";
  }
?>