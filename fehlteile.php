<?php

require('verbindung_aoi.php');     //mit scriptdatei wird hier die db und tabelle ge?ffnet
require('fpdf.php');
$ch = $_GET['ch']; //initialisieren
$ch = str_replace("%", "", $ch);
$ch = str_replace("/", "-", $ch);
$ch = str_replace("?", "_", $ch);
$s_ch = explode("_", $ch);
$start = "";
$end = "";
$n_TXT = "";
$PN = "";
$PCH = "";
$start = $_POST['start']; //initialisieren
$end = $_POST['end']; //initialisieren
$PN = $_POST['PN']; //initialisieren
$PCH = $_POST['CH']; //initialisieren

if ($PCH == "") {
    
} else
    $s_ch[1] = $PCH;

if ($PN == "") {
    
} else
    $s_ch[0] = $PN;

if ($start == "" or $end == "") {
    $earch = " SN like '" . $s_ch[0] . "_" . $s_ch[1] . "%'";
} else {
    $earch = " SN like '" . $s_ch[0] . "_" . $s_ch[1] . "_" . sprintf("%03s", $start) . "%' ";
    for ($i = $start + 1; $i <= $end; $i++) {
        //$earch .= 'or '
        $earch .=" or SN like '" . $s_ch[0] . "_" . $s_ch[1] . "_" . sprintf("%03s", $i) . "%' ";
    }
}/*
  $abfrage = "SELECT u.PN AS PN, COUNT(u.PN) AS ANZAHL
  FROM SPC_Data u INNER JOIN  DefectCode t ON u.DefectCode = t.Code
  WHERE $earch AND Reworked ='0' AND RefID != 'NULL' AND DefectCode ='3' GROUP BY u.PN "; */
$abfrage = "SELECT PN , COUNT(PN) AS ANZAHL
              FROM SPC_Data
              WHERE ( $earch ) AND Reworked ='0' AND RefID != 'NULL' AND( DefectCode ='3' or DefectCode ='4' ) GROUP BY PN ";

$ergebnis = mssql_query($abfrage);

//*
//Fusszeile
class myPDF extends FPDF {

    function Footer() {
        //Position 1,5 cm von unten
        $this->SetY(-15);
        //Arial kursiv 8
        $this->SetFont('Arial', 'I', 8);
        //Seitenzahl
        $this->Cell(0, 10, 'Seite ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}

if ($start != "" or $end != "") {
    $n_TXT = 'Board: ' . $s_ch[0] . ' Charge: ' . $s_ch[1] . ' SN: ' . sprintf("%03s", $start) . ' - ' . sprintf("%03s", $end);
} else {
    $n_TXT = 'Board: ' . $s_ch[0] . ' Charge: ' . $s_ch[1];
}
$pdf = new myPDF();
$pdf->AliasNbPages();
$pdf->AddPage();
//$pdf->SetAutoPageBreak(true);
$pdf->SetFont('Arial', 'B', 10);
$pdf->SetFillColor(100, 100, 255);
$pdf->SetTextColor(0);
$pdf->SetDrawColor(128, 0, 0);
$pdf->SetLineWidth(.3);
//Seitenkopf mit AD logo
$pdf->Image('./GIFs/logo.png', 15, 15, 40);
$pdf->Cell(50, 30, '', 1, 0, 'L', FALSE);
$pdf->Cell(90, 30, $n_TXT, 1, 0, 'C', FALSE);
$pdf->Cell(50, 30, '', 1, 1, 'L', FALSE);
$pdf->SetFont('Arial', 'B', 16);
$pdf->Cell(190, 30, 'Fehlteile', 0, 1, 'C', FALSE);
$pdf->SetFont('Arial', 'B', 8);
$pdf->Ln(6);
//Tabellenkopf
$pdf->SetFillColor(25, 166, 230);
$pdf->Cell(40, 6, 'PN', 'TLR', 0, 'L', true);
$pdf->Cell(30, 6, 'ANZAHL', 'TLR', 0, 'L', true);
$pdf->Cell(120, 6, 'Notiz', 'TLR', 1, 'L', true);

//Tabellenbody
$pdf->SetFillColor(217, 217, 217);
$fill = false;
$ergebnis = mssql_query($abfrage);
while ($row = mssql_fetch_object($ergebnis)) {
    $pdf->Cell(40, 6, $row->PN, 'LR', 0, 'L', $fill);
    $pdf->Cell(30, 6, $row->ANZAHL, 'LR', 0, 'L', $fill);
    $pdf->Cell(120, 6, '', 'LR', 1, 'L', $fill);
    $fill = !$fill;
    //echo $row->PN;
    //echo $row->ANZAHL;
}
$pdf->Cell(190, 0, '', 'T', 1, 'L', FALSE);
$pdf->Ln(6);
$pdf->Cell(95, 12, 'Zusammengestellt von:', 1, 0, 'L', FALSE);
$pdf->Cell(95, 6, 'Entgegengenommen von:', 'LTR', 2, 'L', FALSE);
$pdf->Cell(95, 6, 'Datum:', 'LBR', 1, 'L', FALSE);
//$pdf->MultiCell(95,3,$abfrage,'LBR','L',FALSE);
$pdf->Output();
//*/
?>