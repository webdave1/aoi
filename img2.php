<?php

$ch = $_GET['ch']; //initialisieren
$def = $_GET['def']; //initialisieren
$daten = $_GET['daten']; //initialisieren
require('verbindung_aoi.php');     //mit scriptdatei wird hier die db und tabelle ge?ffnet

$abfrage0 = "SELECT Description FROM DefectCode WHERE Code  = '$def'";
$ergebnis0 = mssql_query($abfrage0);
while ($row = mssql_fetch_object($ergebnis0)) {
    $n0 = $row->Description;
}



diagramm(20, $daten, 800, 350, $ch, $def, $n0);

function diagramm($abstand, $daten, $breite, $hoehe, $ch, $def, $n0) {

    $schrift = 3;
    $legende_abstand = 10;

    $daten = explode(", ", $daten);
    $bezeichnungen = array();
    $werte = array();
    $farben = array();
    $timestamp = time();
    $datum = date("d.m.Y", $timestamp);
    $uhrzeit = date("H:i", $timestamp);
    $jetzt = "$datum.$uhrzeit";

    for ($i = 0; $i < sizeof($daten); $i++) {
        $temp = explode(":", $daten[$i]);
        array_push($bezeichnungen, $temp[0]);
        array_push($werte, $temp[1]);
        array_push($farben, $temp[2]);
        array_push($proz, $temp[3]);
        if ($abstand_text < imagefontwidth($schrift) * strlen($temp[0]))
            $abstand_text = imagefontwidth($schrift) *
                    strlen($temp[0]);
    }
    $abstand_text_h = imagefontheight($schrift);

    $bild = imagecreatetruecolor($breite, $hoehe);


    $farbe_hintergrund = imagecolorexact($bild, 245, 245, 245);
    $farbe_text = imagecolorexact($bild, 0, 0, 0);
    $farbe_text1 = imagecolorexact($bild, 0, 0, 0);
    $farbe_zwischen = imagecolorexact($bild, 220, 220, 220);

    $farbe_rot = imagecolorexact($bild, 255, 0, 0);
    $farbe_gruen = imagecolorexact($bild, 0255, 0, 0);
    $farbe_blau = imagecolorexact($bild, 255, 0, 0);
    $farbe_schwarz = imagecolorexact($bild, 255, 0, 0);
    $farbe_gelb = imagecolorexact($bild, 255, 0, 0);
    $farbe_lila = imagecolorexact($bild, 255, 0, 0);
    $farbe_rot1 = imagecolorexact($bild, 255, 0, 0);
    $farbe_gruen1 = imagecolorexact($bild, 255, 0, 0);
    $farbe_blau1 = imagecolorexact($bild, 0, 255, 0, 0);
    $farbe_schwarz1 = imagecolorexact($bild, 255, 0, 0);
    $farbe_gelb1 = imagecolorexact($bild, 255, 0, 0);
    $farbe_lila1 = imagecolorexact($bild, 255, 0, 0);
    $farbe_rot2 = imagecolorexact($bild, 255, 0, 0);
    $farbe_gruen2 = imagecolorexact($bild, 255, 0, 0);
    $farbe_blau2 = imagecolorexact($bild, 255, 0, 0);
    $farbe_schwarz2 = imagecolorexact($bild, 255, 0, 0);
    $farbe_gelb2 = imagecolorexact($bild, 255, 0, 0);
    $farbe_lila2 = imagecolorexact($bild, 255, 0, 0);

    imagefill($bild, 0, 0, $farbe_hintergrund);
    $linie_x = $abstand;
    $linie_b = ($breite - 2 * $abstand) /
            (sizeof($werte) - 1);
    $linie_h = $hoehe - 2 * $abstand;
    $linie_versatz = 0;

    $punkte = array();

    for ($i = 0; $i < sizeof($werte); $i++) {

        $hoechstwert = $werte;
        rsort($hoechstwert, SORT_NUMERIC);
        $hoechstwert = $hoechstwert[0];

        $prozent = 90 / $hoechstwert *
                $werte[$i];
        $linie_y = $linie_h / 100 *
                $prozent;

        $farbe = "farbe_" . $farben[$i];

        array_push($punkte, $linie_x +
                $linie_versatz, $linie_h - $linie_y + $abstand);

        $linie_versatz = $linie_versatz + $linie_b;
    }

    array_push($punkte, $breite - $abstand, $hoehe - $abstand, $abstand, $hoehe - $abstand);

    $farbe = "farbe_rot";
    imagefilledpolygon($bild, $punkte, sizeof($punkte) / 2, ${$farbe});                                 //Der Verlauf

    imageline($bild, $abstand, $abstand, $abstand, $hoehe - $abstand, $farbe_text);                     //Y-Achse
    imageline($bild, $abstand, $hoehe - $abstand, $breite - $abstand, $hoehe - $abstand, $farbe_text);  //X-Achse
    imagestring($bild, $schrift, $abstand + 4, $abstand, $einheit, $farbe_text);

    for ($i = 100; $i >= 0; $i = $i - 10) {
        $prozent = 100 / $hoechstwert * $i;
        $y = $linie_h - round($linie_h /
                        100 * $prozent);
        //imageline($bild, $abstand, $abstand + $y, $abstand + 10, $abstand + $y, $farbe_text);           //Maß-Striche
    }

    $linie_versatz = 0;

    /*  for($i=0; $i<sizeof($werte); $i++) {
      imageline($bild, $linie_x +
      $linie_versatz, $hoehe - $abstand - 10, $linie_x + $linie_versatz,
      $hoehe - $abstand, $farbe_text);
      if($i < sizeof($werte) - 1)
      imagestring($bild, $schrift, $linie_x + $linie_versatz, $hoehe - $abstand + 20 - 2,
      $bezeichnungen[$i]."(".$werte[$i].")", $farbe_text1);
      $linie_versatz = $linie_versatz + $linie_b;//Bezeichnung */


    for ($i = 0; $i < sizeof($werte); $i++) {
        imageline($bild, $linie_x +
                $linie_versatz, $hoehe - $abstand - 10, $linie_x + $linie_versatz, $hoehe - $abstand, $farbe_text);
        if ($i < sizeof($werte))
            imagestringup($bild, $schrift, $linie_x + $linie_versatz + 2, $hoehe -
                    $abstand - 10 - 2, $bezeichnungen[$i] . $werte[$i], $farbe_text);
        $linie_versatz = $linie_versatz +
                $linie_b;
    }


    imagestring($bild, '18', '30', '2', $ch, $farbe_text);
    imagestring($bild, '18', '30', '15', $n0, $farbe_text);
    imagestring($bild, '1', '10', '342', "Quelle AOI_Protokoll by Dave. Avionic-Design" . " " . $jetzt, $farbe_text);



    header("Content-type: image/png");
    imagepng($bild);
}

?>
