var PIC_t;              //Mouseevent Coordinate Top
var PIC_l;              //Mouseevent Coordinate Left
var PIC_st;             //Bild Coordinate bei Start Mousebewegung Top
var PIC_sl;             //Bild Coordinate bei Start Mousebewegung Left
var PIC_mt;             //Neue Bild Position Top
var PIC_ml;             //Neue Bild Position Left
var PIC_mt_div = 0;     //Mouse bewegt um xxpx Top
var PIC_ml_div = 0;     //Mouse bewegt um xxpx Left
var PIC_move = 0;     //Bild schieben
var List_move = 0;     //Liste schieben
var ImgAnz = 0;     //Anzahl der Bilder (Recipes)
var ListCount = 0;     //Laenge der Fehler Liste
var zoom = 0;     //Zoom zustand
var dbclk = 0;     //Doppeltap zustand
var T_dbclk = 0;     //Timeout fuer doppeltap
var swipe = 0;     //Mousedown (Finger auf dem Touch) wichtig fuer PicMove.


function PicMoveStart(e) {

    //Timeout Stoppen
    clearTimeout(T_dbclk);

    //Wenn Doppelclick dann Zoomen
    if (dbclk == 1)
        zoomin(e);
    else {
        //Anzahl der Bilder (Recipes) auslesen
        ImgAnz = parseInt(document.getElementById('ImgAnz').innerHTML, 10);
        //console.log('ImgAnz: '+document.getElementById('ImgAnz').innerHTML);

        //MousePosition holen
        var p = mouse_pos(e);
        PIC_t = parseInt(p['top'], 10);
        PIC_l = parseInt(p['left'], 10);

        //Position Grafik holen
        PIC_st = parseInt(e.target.style.top, 10);
        PIC_sl = parseInt(e.target.style.left, 10);

        //evtl. Verschieben freischalten
        if (zoom != 0) {
            PIC_move = 1;

        }
    }
    swipe = 1;
    dbclk = 1;
}

function PicMove(e) {
    //MousePosition holen
    var p = mouse_pos(e);
    //console.log('Maus:'+p['left']+'Wischen:'+swipe)
    //Wenn mit gedrueckter mouse /Finger auf dem Touch)
    if (swipe == 1) {
        //Schiebefenstergroesse holen
        var tpnsw = parseInt(e.target.parentNode.style.width, 10);
        var tpnsh = parseInt(e.target.parentNode.style.height, 10);

        //Bildschiebedistanz ermitteln
        PIC_mt_div = (parseInt(p['top'], 10) - PIC_t);
        PIC_ml_div = (parseInt(p['left'], 10) - PIC_l);

        //Wenn Verschieben freigeschaltet
        if (PIC_move == 1) {

            //Neue Bildposition errechnen
            PIC_mt = PIC_st + PIC_mt_div;
            PIC_ml = PIC_sl + PIC_ml_div;

            //Neue Bild Position validieren
            if (PIC_mt >= 0)
                PIC_mt = 0;
            if (PIC_mt <= '-' + (tpnsh * zoom))
                PIC_mt = '-' + (tpnsh * zoom);
            if (PIC_ml >= 0)
                PIC_ml = 0;
            if (PIC_ml <= '-' + (tpnsw * zoom))
                PIC_ml = '-' + (tpnsw * zoom);

            //Bild Verschieben
            e.target.style.top = PIC_mt + "px";
            e.target.style.left = PIC_ml + "px";
            //console.log('top:'+PIC_mt+' left:'+PIC_ml)
        }
    }
}

function PicMoveStop(e) {
    //Doppelclick eingrenzen (Zeitlich)
    T_dbclk = setTimeout("dbclk = 0;", 400);

    //Verschieben beenden
    if (PIC_move == 1)
        PIC_move = 0;
    else {
        //wenn verschieben nicht freigeschaltet
        if (e) {
            //Bild ID ermitteln
            var ImgIDInt = parseInt(e.target.id, 10);

            //Wenn Wischbewegung min 150px nach rechts und Aktuelle grafik min. [2I]
            if ((PIC_ml_div >= 150) && (ImgIDInt >= 2) && (swipe == 1)) {
                //Vorherige Grafik einblenden
                document.getElementById((ImgIDInt - 1) + 'I').style.visibility = 'visible';
                dbclk = 0;
            }
            //Wenn Wischbewegung min 150px nach links und Aktuelle grafik nicht [die Letzte verfuegbare Grafik [1I]
            else if ((PIC_ml_div <= -150) && (ImgIDInt <= ImgAnz - 1) && (swipe == 1)) {
                //aktuelle Grafik ausblenden
                document.getElementById(ImgIDInt + 'I').style.visibility = 'hidden';
                dbclk = 0;
            }
        }
    }
    swipe = 0;
    PIC_ml_div = 0;
}


function mouse_pos(e) {
    if (!e)
        e = window.event;
    var body = (window.document.compatMode && window.document.compatMode == "CSS1Compat") ?
            window.document.documentElement : window.document.body;
    return {
        // Position im Dokument
        top: e.pageY ? e.pageY : e.clientY + body.scrollTop - body.clientTop,
        left: e.pageX ? e.pageX : e.clientX + body.scrollLeft - body.clientLeft
    };
}


function zoomin(e) {
    //console.log(e.target.name)
    dbclk = 0;
    //offset des Cursors ermitteln wenn das eigendliche element absolute und 0px ist.
    x = e.pageX - e.target.parentNode.offsetLeft;
    y = e.pageY - e.target.parentNode.offsetTop;
    switch (zoom) {

        case 0:
            //Grafik 2X ZOOM
            e.target.style.top = (parseInt(e.target.style.top, 10) - parseInt(y, 10)) + "px";
            e.target.style.left = (parseInt(e.target.style.left, 10) - parseInt(x, 10)) + "px";
            e.target.style.width = parseInt(e.target.style.width, 10) * 2 + "px";
            zoom = 1;
            //console.log('Zoom'+zoom+' width '+e.target.style.width);
            break;
        case 1:
            //Grafik 3X ZOOM
            e.target.style.top = (parseInt(e.target.style.top, 10) - parseInt(y, 10)) + "px";
            e.target.style.left = (parseInt(e.target.style.left, 10) - parseInt(x, 10)) + "px";
            e.target.style.width = parseInt(e.target.style.width, 10) * 1.5 + "px";
            zoom = 2;
            //console.log('Zoom'+zoom+' width '+e.target.style.width);
            break;
        default:
            //Grafik 1X ZOOM
            zoom = 0;
            e.target.style.top = "0px";
            e.target.style.left = "0px";
            e.target.style.width = parseInt(e.target.style.width, 10) / 3 + "px";
            //console.log('Zoom'+zoom+'  width '+e.target.style.width);
            break;
    }
}

function ChekOut(row, RwRes)
{
    var RIndex = row;
    var ReviewResult = RwRes;
    var SendResult = new XMLHttpRequest();
    SendResult.open("GET", './bearbeitet.php?' + RwRes, false);
    SendResult.send(null);
    //console.log('PHP abfrage liefert: '+SendResult.responseText);
    if (SendResult.responseText == 'true')
        document.getElementById("defectlist").deleteRow(RIndex);
    ListCount = parseInt(document.getElementById("defectlist").rows.length, 10) - 9;
    if (ListCount == 0) {
        //console.log('Back to Zerro!!!')
        document.getElementById("defectlist").style.top = "0px";
    }
    Check();
}

//Fokus in eingabefeld
function Check()
{
    document.SN.sn.focus();
}

function ListMoveStart(e) {
    var p = mouse_pos(e);
    ListCount = parseInt(document.getElementById("defectlist").rows.length, 10) - 9;
    if (ListCount >= 1)
        List_move = 1;
    List_t = parseInt(p['top'], 10);
    List_st = parseInt(document.getElementById("defectlist").style.top, 10);
}

function ListMove(e) {
    var p = mouse_pos(e);
    if (List_move == 1) {
        List_mt_div = (parseInt(p['top'], 10) - List_t);
        List_mt = List_st + List_mt_div;
        if (List_mt >= 0)
            List_mt = 0;
        if (List_mt <= '-' + (ListCount * 70))
            List_mt = '-' + (ListCount * 70);
        document.getElementById("defectlist").style.top = List_mt + "px";
    }
}

function ListMoveStop(e) {
    if (List_move == 1)
        List_move = 0;
}