<?php

$ch = $_GET['ch']; //initialisieren
$proz = $_GET['proz'];
$proz = explode(", ", $proz);
$anz = $_GET['anz'];
$fehler = $_GET['fehler'];
$parts = $_GET['parts'];
$pn = $ch;
$menge = $anz;
//$n0 = $_GET['n0'] ;
//$w0 = $_GET['w0'] ;
$n1 = $_GET['n1'];
$w1 = $_GET['w1'];
$n2 = $_GET['n2'];
$w2 = $_GET['w2'];
$n3 = $_GET['n3'];
$w3 = $_GET['w3'];
$n4 = $_GET['n4'];
$w4 = $_GET['w4'];
$n5 = $_GET['n5'];
$w5 = $_GET['w5'];
$n6 = $_GET['n6'];
$w6 = $_GET['w6'];
$n7 = $_GET['n7'];
$w7 = $_GET['w7'];
$n8 = $_GET['n8'];
$w8 = $_GET['w8'];
$n9 = $_GET['n9'];
$w9 = $_GET['w9'];
$n10 = $_GET['n10'];
$w10 = $_GET['w10'];
$n11 = $_GET['n11'];
$w11 = $_GET['w11'];
$n12 = $_GET['n12'];
$w12 = $_GET['w12'];
$n13 = $_GET['n13'];
$w13 = $_GET['w13'];
$n14 = $_GET['n14'];
$w14 = $_GET['w14'];
$n15 = $_GET['n15'];                                                                                                                                                                                                //rot,gruen,lila,blau,gelb,schwarz
$w15 = $_GET['w15'];
$n16 = $_GET['n16'];
$w16 = $_GET['w16'];
$x100 = "100";
$p0 = number_format($w0 * $x100 / $parts, 2);
$p1 = number_format($w1 * $x100 / $parts, 2);
$p2 = number_format($w2 * $x100 / $parts, 2);
$p3 = number_format($w3 * $x100 / $parts, 2);
$p4 = number_format($w4 * $x100 / $parts, 2);
$p5 = number_format($w5 * $x100 / $parts, 2);
$p6 = number_format($w6 * $x100 / $parts, 2);
$p7 = number_format($w7 * $x100 / $parts, 2);
$p8 = number_format($w8 * $x100 / $parts, 2);
$p9 = number_format($w9 * $x100 / $parts, 2);
$p10 = number_format($w10 * $x100 / $parts, 2);
$p11 = number_format($w11 * $x100 / $parts, 2);
$p12 = number_format($w12 * $x100 / $parts, 2);
$p13 = number_format($w13 * $x100 / $parts, 2);
$p14 = number_format($w14 * $x100 / $parts, 2);
$p15 = number_format($w15 * $x100 / $parts, 2);
$p16 = number_format($w16 * $x100 / $parts, 2);
$timestamp = time();
$datum = date("d.m.Y", $timestamp);
$uhrzeit = date("H:i", $timestamp);
$jetzt = "$datum.$uhrzeit";




diagramm("balken", 10, "$n1:$w1:rot1:$p1, $n2:$w2:rot2:$p2, $n3:$w3:gruen:$p3, $n4:$w4:gruen1:$p4, $n5:$w5:gruen2:$p5, $n6:$w6:lila:$p6, $n7:$w7:lila1:$p7, $n8:$w8:lila2:$p8, $n9:$w9:blau:$p9, $n10:$w10:blau1:$p10, $n11:$w11:blau2:$p11, $n12:$w12:gelb:$p12, $n13:$w13:gelb1:$p13, $n14:$w14:gelb2:$p14, $n15:$w15:schwarz:$p15, $n16:$w16:schwarz1:$p16", "", 800, 350, $ch, $anz, $fehler, $parts, $jetzt);

function diagramm($typ, $abstand, $daten, $einheit, $breite, $hoehe, $pn, $menge, $fehler, $parts, $jetzt) {

    $schrift = 3;
    $legende_abstand = 10;

    $daten = explode(", ", $daten);
    $werte = array();
    $bezeichnungen = array();
    $farben = array();
    $proz = array();

    for ($i = 0; $i < sizeof($daten); $i++) {
        $temp = explode(":", $daten[$i]);
        array_push($bezeichnungen, $temp[0]);
        array_push($werte, $temp[1]);
        array_push($farben, $temp[2]);
        array_push($proz, $temp[3]);
        if ($abstand_text < imagefontwidth($schrift) * strlen($temp[0]))
            $abstand_text = imagefontwidth($schrift) *
                    strlen($temp[0]);
    }
    $abstand_text_h = imagefontheight($schrift);

    $bild = imagecreatetruecolor($breite, $hoehe);


    $farbe_hintergrund = imagecolorexact($bild, 245, 245, 245);
    $farbe_text = imagecolorexact($bild, 0, 0, 0);
    $farbe_text1 = imagecolorexact($bild, 255, 0, 0);
    $farbe_zwischen = imagecolorexact($bild, 220, 220, 220);

    $farbe_rot = imagecolorexact($bild, 184, 184, 184);
    $farbe_gruen = imagecolorexact($bild, 0, 255, 0);
    $farbe_blau = imagecolorexact($bild, 0, 0, 255);
    $farbe_schwarz = imagecolorexact($bild, 0, 0, 0);
    $farbe_gelb = imagecolorexact($bild, 255, 255, 0);
    $farbe_lila = imagecolorexact($bild, 255, 0, 255);
    $farbe_rot1 = imagecolorexact($bild, 205, 16, 118);
    $farbe_gruen1 = imagecolorexact($bild, 188, 238, 104);
    $farbe_blau1 = imagecolorexact($bild, 0, 206, 209);
    $farbe_schwarz1 = imagecolorexact($bild, 50, 50, 50);
    $farbe_gelb1 = imagecolorexact($bild, 205, 149, 12);
    $farbe_lila1 = imagecolorexact($bild, 153, 50, 204);
    $farbe_rot2 = imagecolorexact($bild, 255, 182, 193);
    $farbe_gruen2 = imagecolorexact($bild, 160, 175, 127);
    $farbe_blau2 = imagecolorexact($bild, 104, 131, 139);
    $farbe_schwarz2 = imagecolorexact($bild, 155, 155, 155);
    $farbe_gelb2 = imagecolorexact($bild, 139, 129, 76);
    $farbe_lila2 = imagecolorexact($bild, 171, 130, 255);

    imagefill($bild, 0, 0, $farbe_hintergrund);
    $balken_x = $abstand;
    $balken_y = $hoehe - $abstand;
    $balken_b = 2 * $abstand;
    $diagramm_h = $hoehe - 2 * $abstand;
    $balken_versatz = 0;

    $legende_x = $balken_x + sizeof($werte) *
            $balken_b + (sizeof($werte) - 1) * $abstand + 2 * $abstand;
    $legende_y = $hoehe - $abstand -
            $legende_abstand;
    $legende_b = $legende_x + $legende_abstand;
    $legende_h = $legende_y + $legende_abstand;
    $legende_versatz = 0;

    for ($i = 0; $i < sizeof($werte); $i++) {

        $prozent = 100 / array_sum($werte) * $werte[$i];
        $balken_h = $diagramm_h / 100 *
                $prozent;

        $wert = $werte[$i] . " " . $einheit . " (" . $proz[$i] . "%)";

        $farbe = "farbe_" . $farben[$i];
        imagefilledrectangle($bild, $balken_x + $balken_versatz, $abstand, $balken_x + $balken_versatz +
                $balken_b, $hoehe - $abstand, $farbe_zwischen);
        imagefilledrectangle($bild, $balken_x + $balken_versatz, $balken_y - $balken_h, $balken_x +
                $balken_versatz + $balken_b, $balken_y, ${$farbe});

        imagestring($bild, $schrift, $balken_x + $balken_versatz + 2, $balken_y - $balken_h -
                $abstand_text_h, $werte[$i], $farbe_text);

        imagefilledrectangle($bild, $legende_x, $legende_y - $legende_versatz, $legende_b, $legende_h -
                $legende_versatz, ${$farbe});
        imagestring($bild, $schrift, $legende_x + 2 * $legende_abstand, $legende_y - $legende_versatz, $bezeichnungen[$i], $farbe_text);
        imagestring($bild, $schrift, $legende_x + 3 * $legende_abstand + $abstand_text, $legende_y -
                $legende_versatz, $wert, $farbe_text);
        $balken_versatz = $balken_versatz +
                3 * $abstand;
        $legende_versatz = $legende_versatz + 2 * $legende_abstand;
    }
    imagestring($bild, '18', '2', '2', "Charge:", $farbe_text);
    imagestring($bild, '18', '90', '2', $pn, $farbe_text);
    imagestring($bild, '18', '2', '15', "Platinen:", $farbe_text);
    imagestring($bild, '18', '90', '15', $menge, $farbe_text);
    imagestring($bild, '18', '2', '28', "Bauteile:", $farbe_text);
    imagestring($bild, '18', '90', '28', $parts, $farbe_text);
    imagestring($bild, '18', '2', '41', "Fehler:", $farbe_text);
    imagestring($bild, '18', '90', '41', $fehler, $farbe_text);
    imagestring($bild, '1', '2', '342', "Quelle AOI_Protokoll by Dave. Avionic-Design" . " " . $jetzt, $farbe_text);



    header("Content-type: image/png");
    imagepng($bild);
}

?>
